import requests
import scrapy
import os
from bs4 import BeautifulSoup

urls=[
['https://oshpd.ca.gov/data-and-reports/cost-transparency/hospital-chargemasters/latest-chargemasters/', '', "", "California"]

]

class CaliforniaSpider(scrapy.Spider):
    name = 'california'
    def start_requests(self):
        for url in urls:
            yield scrapy.Request(url=url[0], callback=self.parse,meta={'index':url[1],'name':url[2],'state':url[3]})

    def parse(self, response):
      records = []
      soup = BeautifulSoup(response.text, 'lxml')
      entry = soup.find("tbody", {"class": "row-hover"})
      children = entry.findChildren("tr", recursive=False)
      for child in children:
        entry_name = child.find(class_='column-2').text
        link = child.find(class_='column-3').find('a', href=True)
        link=link['href']
        if '.xls' in link.lower():
            self.logger.info(link)
            outdir = os.path.join("Data")
            if os.path.isdir(outdir) == False:
                os.mkdir(outdir)
            path = os.path.join(outdir, 'California')
            if os.path.isdir(path) == False:
                os.mkdir(path)
            path = os.path.join(path, entry_name)
            if os.path.isdir(path) == False:
                os.mkdir(path)
            self.log("\n\n We got data! \n\n" + path)
            self.logger.info('Saving File %s', path)
            r = requests.get(link, headers={
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.100 Safari/537.36'})
            filename = link.split("/")[-1].split("?")[0]
            path = os.path.join(path, filename)
            with open(path, 'wb') as f:
                f.write(r.content)
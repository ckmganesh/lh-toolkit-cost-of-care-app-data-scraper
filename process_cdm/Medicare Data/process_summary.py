
import os
import sys
import pandas
sys.path.append(os.getcwd())
from ParseData import ParseData

a = ParseData()
here = os.path.dirname(os.path.abspath(__file__))
folder = os.path.basename(here)
basePath=here.split("process_cdm")[0]
destination=os.path.join(basePath,"Summary_Data")
if not os.path.exists(destination):
    os.mkdir(destination)
pathToData=os.path.join(basePath,'Data')
here=os.path.join(os.path.join(pathToData,'Medicare Data'),"Inpatient_Summary")
if not os.path.exists(here):
    print("No Data Found For {0}".format(here))
    exit(0)

filePath=os.path.join(here,"rows.csv")
content = pandas.read_csv(filePath,usecols=['DRG_DESC', 'AVERAGE_MEDICARE_PAYMENTS'],encoding= 'unicode_escape')
content.rename(columns={'AVERAGE_MEDICARE_PAYMENTS': 'Charge', 'DRG_DESC': 'Description'}, inplace=True)

content['Description']=content['Description'].str.split('-').str[1]
content = content.dropna(how='all')
content.to_json(os.path.join(destination,'inpatient_summary.json'), orient='records')

#Reading Outpateint file
here=os.path.join(os.path.join(pathToData,'Medicare Data'),"Outpatient_Summary")
filePath=os.path.join(here,"rows.csv")
out = pandas.read_csv(filePath,usecols=['APC_Desc', 'Average_Medicare_Payment_Amount'],encoding= 'unicode_escape')
out.rename(columns={'Average_Medicare_Payment_Amount': 'Charge','APC_Desc': 'Description'}, inplace=True)

out.to_json(os.path.join(destination,'outpatient_summary.json'),orient='records')